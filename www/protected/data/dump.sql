-- phpMyAdmin SQL Dump
-- version 3.2.3
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 02, 2012 at 02:48 PM
-- Server version: 5.1.40
-- PHP Version: 5.3.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `testSite`
--

-- --------------------------------------------------------

--
-- Table structure for table `grades`
--

CREATE TABLE IF NOT EXISTS `grades` (
  `gradeId` int(11) NOT NULL AUTO_INCREMENT,
  `gradeName` varchar(255) NOT NULL,
  PRIMARY KEY (`gradeId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `grades`
--

INSERT INTO `grades` (`gradeId`, `gradeName`) VALUES
(1, 'ege');

-- --------------------------------------------------------

--
-- Table structure for table `learning`
--

CREATE TABLE IF NOT EXISTS `learning` (
  `learningId` int(11) NOT NULL AUTO_INCREMENT,
  `learningText` text NOT NULL,
  PRIMARY KEY (`learningId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `learning`
--

INSERT INTO `learning` (`learningId`, `learningText`) VALUES
(1, 'first learning'),
(2, 'second learning'),
(3, 'third learning');

-- --------------------------------------------------------

--
-- Table structure for table `levels`
--

CREATE TABLE IF NOT EXISTS `levels` (
  `levelId` int(11) NOT NULL AUTO_INCREMENT,
  `levelName` varchar(255) NOT NULL,
  PRIMARY KEY (`levelId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `levels`
--

INSERT INTO `levels` (`levelId`, `levelName`) VALUES
(1, 'simple');

-- --------------------------------------------------------

--
-- Table structure for table `problemAnswers`
--

CREATE TABLE IF NOT EXISTS `problemAnswers` (
  `problemId` int(11) NOT NULL,
  `rightAnswer` text NOT NULL,
  `wrongAnswer1` text NOT NULL,
  `additionalTask1` int(11) NOT NULL,
  `wrongAnswer2` text NOT NULL,
  `additionalTask2` int(11) NOT NULL,
  `wrongAnswer3` text NOT NULL,
  `additionalTask3` int(11) NOT NULL,
  `allAdditionalTasks` varchar(255) NOT NULL,
  KEY `(foreign-key4)` (`problemId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `problemAnswers`
--

INSERT INTO `problemAnswers` (`problemId`, `rightAnswer`, `wrongAnswer1`, `additionalTask1`, `wrongAnswer2`, `additionalTask2`, `wrongAnswer3`, `additionalTask3`, `allAdditionalTasks`) VALUES
(1, 'right', '1', 3, '2', 4, '3', 5, '3,4,5'),
(2, 'right answer for second task', '123', 0, '234', 0, '345', 0, ''),
(3, 'right answer for 3 task', 'wrong 1 answer for 3 task', 1, 'wrong 2 answer for 3 task', 2, 'wrong 3 answer for 3 task', 3, ''),
(4, 'right answer for 4 task', 'wrong 1 answer for 4 task', 0, 'wrong 2 answer for 4 task', 0, 'wrong 3 answer for 4 task', 0, ''),
(5, 'right answer for 5 task', 'wrong 1 answer for 5 task', 0, 'wrong 2 answer for 5 task', 0, 'wrong 3 answer for 5 task', 0, ''),
(7, 'r a', 'w a 1', 3, 'sdf', 4, 'ret', 5, '3,4,5');

-- --------------------------------------------------------

--
-- Table structure for table `problems`
--

CREATE TABLE IF NOT EXISTS `problems` (
  `problemId` int(11) NOT NULL AUTO_INCREMENT,
  `themeId` int(11) NOT NULL,
  `testProblemId` int(11) NOT NULL,
  `egePartId` varchar(255) CHARACTER SET utf8 COLLATE utf8_estonian_ci NOT NULL,
  `text` text NOT NULL,
  `problemName` varchar(255) NOT NULL,
  `mistake` int(11) NOT NULL,
  `taskType` int(11) NOT NULL,
  `learningId` int(11) NOT NULL,
  `testId` int(11) NOT NULL,
  PRIMARY KEY (`problemId`),
  KEY `(foreign-key)` (`themeId`),
  KEY `(foreign-key2)` (`testId`),
  KEY `(foreign-key3)` (`learningId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `problems`
--

INSERT INTO `problems` (`problemId`, `themeId`, `testProblemId`, `egePartId`, `text`, `problemName`, `mistake`, `taskType`, `learningId`, `testId`) VALUES
(1, 1, 1, '', 'first task', '', 0, 0, 1, 1),
(2, 1, 2, '', 'second task', '', 0, 0, 1, 1),
(3, 1, 0, '', 'problem if first wrong answer', '', 1, 0, 1, 1),
(4, 1, 0, '', 'problem if second wrong answer', '', 1, 0, 1, 1),
(5, 1, 0, '', 'problem if third wrong answer', '', 1, 0, 1, 1),
(7, 1, 3, '', 'third task', '', 0, 0, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

CREATE TABLE IF NOT EXISTS `subjects` (
  `subjectId` int(11) NOT NULL AUTO_INCREMENT,
  `subjectName` varchar(255) NOT NULL,
  PRIMARY KEY (`subjectId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `subjects`
--

INSERT INTO `subjects` (`subjectId`, `subjectName`) VALUES
(1, 'math');

-- --------------------------------------------------------

--
-- Table structure for table `tests`
--

CREATE TABLE IF NOT EXISTS `tests` (
  `testId` int(11) NOT NULL AUTO_INCREMENT,
  `subjectId` int(11) NOT NULL,
  `levelId` int(11) NOT NULL,
  `gradeId` int(11) NOT NULL,
  PRIMARY KEY (`testId`),
  KEY `(foreign-key5)` (`subjectId`),
  KEY `(foreign-key6)` (`levelId`),
  KEY `(foreign-key7)` (`gradeId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tests`
--

INSERT INTO `tests` (`testId`, `subjectId`, `levelId`, `gradeId`) VALUES
(1, 1, 1, 1),
(2, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `themes`
--

CREATE TABLE IF NOT EXISTS `themes` (
  `themeId` int(11) NOT NULL AUTO_INCREMENT,
  `themeName` varchar(255) NOT NULL,
  PRIMARY KEY (`themeId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `themes`
--

INSERT INTO `themes` (`themeId`, `themeName`) VALUES
(1, 'log');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `userId` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `teacher` tinyint(4) NOT NULL,
  `admin` tinyint(4) NOT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `users`
--


--
-- Constraints for dumped tables
--

--
-- Constraints for table `problemAnswers`
--
ALTER TABLE `problemAnswers`
  ADD CONSTRAINT `(foreign-key4)` FOREIGN KEY (`problemId`) REFERENCES `problems` (`problemId`) ON DELETE CASCADE,
  ADD CONSTRAINT `(problem)` FOREIGN KEY (`problemId`) REFERENCES `problems` (`problemId`) ON DELETE CASCADE;

--
-- Constraints for table `problems`
--
ALTER TABLE `problems`
  ADD CONSTRAINT `(foreign-key)` FOREIGN KEY (`themeId`) REFERENCES `themes` (`themeId`) ON DELETE CASCADE,
  ADD CONSTRAINT `(foreign-key2)` FOREIGN KEY (`testId`) REFERENCES `tests` (`testId`) ON DELETE CASCADE,
  ADD CONSTRAINT `(foreign-key3)` FOREIGN KEY (`learningId`) REFERENCES `learning` (`learningId`) ON DELETE CASCADE,
  ADD CONSTRAINT `(learning)` FOREIGN KEY (`learningId`) REFERENCES `learning` (`learningId`) ON DELETE CASCADE,
  ADD CONSTRAINT `(test)` FOREIGN KEY (`testId`) REFERENCES `tests` (`testId`) ON DELETE CASCADE,
  ADD CONSTRAINT `(themes)` FOREIGN KEY (`themeId`) REFERENCES `themes` (`themeId`) ON DELETE CASCADE;

--
-- Constraints for table `tests`
--
ALTER TABLE `tests`
  ADD CONSTRAINT `(foreign-key5)` FOREIGN KEY (`subjectId`) REFERENCES `subjects` (`subjectId`) ON DELETE CASCADE,
  ADD CONSTRAINT `(foreign-key6)` FOREIGN KEY (`levelId`) REFERENCES `levels` (`levelId`) ON DELETE CASCADE,
  ADD CONSTRAINT `(foreign-key7)` FOREIGN KEY (`gradeId`) REFERENCES `grades` (`gradeId`) ON DELETE CASCADE,
  ADD CONSTRAINT `(grade)` FOREIGN KEY (`gradeId`) REFERENCES `grades` (`gradeId`) ON DELETE CASCADE,
  ADD CONSTRAINT `(level)` FOREIGN KEY (`levelId`) REFERENCES `levels` (`levelId`) ON DELETE CASCADE,
  ADD CONSTRAINT `(subject)` FOREIGN KEY (`subjectId`) REFERENCES `subjects` (`subjectId`) ON DELETE CASCADE;
