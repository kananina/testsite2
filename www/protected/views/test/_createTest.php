<?php
$form=$this->beginWidget('CActiveForm', array(
	'id'=>'form',
	'enableAjaxValidation'=>false,
        'clientOptions'=>array(
                'validateOnSubmit'=>true,
        ),
));
    echo CHtml::hiddenField('testId',$testId);
    echo CHtml::hiddenField('problemId',$problemId);
    ?>
    Problem Text:
    <br/>
    <?php
    echo CHtml::textArea('problemText','');
    ?>
    <br/><br/><br/><br/><br/><br/>
    
    Write Answer:
    <br/>
    <?php
    echo CHtml::textField('writeAnswer','');
    ?>
    <br/><br/><br/><br/>
    First wrong answer:
    <br/>
    <?php
    echo CHtml::textField('wrongAnswer1','');
    ?>

    <br/>
    Additional problem  �1:
    <br/>
    <?php
    echo CHtml::textField('additionalTask1','');
    ?>
    <br/><br/><br/>
    Write Answer for additional problem �1:
    <br/>
    <?php
    echo CHtml::textField('writeAnswerAdditional1','');
    ?>
    <br/>
    First wrong answer for additional problem �1:
    <br/>
    <?php
    echo CHtml::textField('wrongAnswer1Additional1','');
    ?>
    <br/>
    Learning for additional problem �1:
    <br/>
    <?php
    echo CHtml::textField('learning1','');
    ?>
    <br/>
    Second wrong answer for additional problem �1:
    <br/>
    <?php
    echo CHtml::textField('wrongAnswer2Additional1','');
    ?>
    <br/>
    Learning for additional problem �1:
    <br/>
    <?php
    echo CHtml::textField('learning2','');
    ?>
    <br/>
    Third wrong answer for additional problem �1:
    <br/>
    <?php
    echo CHtml::textField('wrongAnswer3Additional1','');
    ?>
    <br/>
    Learning for additional problem �1:
    <br/>
    <?php
    echo CHtml::textField('learning3','');
    ?>
    <br/><br/><br/><br/>
    Second wrong answer:
    <br/>
    <?php
    echo CHtml::textField('wrongAnswer2','');
    ?>
    <br/>
    Additional problem �2:
    <br/>
    <?php
    echo CHtml::textField('additionalTask2','');
    ?>
    <br/><br/><br/>
    Write Answer for additional problem �2:
    <br/>
    <?php
    echo CHtml::textField('writeAnswerAdditional2','');
    ?>
    <br/>
    First wrong answer for additional problem �2:
    <br/>
    <?php
    echo CHtml::textField('wrongAnswer1Additional2','');
    ?>
    <br/>
    Learning for additional problem �2:
    <br/>
    <?php
    echo CHtml::textField('learning4','');
    ?>
    <br/>
    Second wrong answer for additional problem �2:
    <br/>
    <?php
    echo CHtml::textField('wrongAnswer2Additional2','');
    ?>
    <br/>
    Learning for additional problem �2:
    <br/>
    <?php
    echo CHtml::textField('learning5','');
    ?>
    <br/>
    Third wrong answer for additional problem �2:
    <br/>
    <?php
    echo CHtml::textField('wrongAnswer3Additional2','');
    ?>
    <br/>
    Learning for additional problem �2:
    <br/>
    <?php
    echo CHtml::textField('learning6','');
    ?>
    <br/><br/><br/><br/>
    Third wrong answer:
    <br/>
    <?php
    echo CHtml::textField('wrongAnswer3','');
    ?>
    <br/>
    Additional problem �3:
    <br/>
    <?php
    echo CHtml::textField('additionalTask3','');
    ?>
    <br/><br/><br/>
    Write Answer for additional problem �3:
    <br/>
    <?php
    echo CHtml::textField('writeAnswerAdditional3','');
    ?>
    <br/>
    First wrong answer for additional problem �3:
    <br/>
    <?php
    echo CHtml::textField('wrongAnswer1Additional3','');
    ?>
    <br/>
    Learning for additional problem �3:
    <br/>
    <?php
    echo CHtml::textField('learning7','');
    ?>
    <br/>
    Second wrong answer for additional problem �3:
    <br/>
    <?php
    echo CHtml::textField('wrongAnswer2Additional3','');
    ?>
    <br/>
    Learning for additional problem �3:
    <br/>
    <?php
    echo CHtml::textField('learning8','');
    ?>
    <br/>
    Third wrong answer for additional problem �3:
    <br/>
    <?php
    echo CHtml::textField('wrongAnswer3Additional3','');
    ?>
    <br/>
    Learning for additional problem �3:
    <br/>
    <?php
    echo CHtml::textField('learning9','');
    ?>
    <br/>
    <?php
    echo CHtml::ajaxSubmitButton('Create problem', array('test/index'),
            array(
                'type' => 'POST',
                'update' => '#task',
                            ),
            array(
                'id' => 'createButton'.$problem->testProblemId,
                'type' => 'submit',
            ));
$this->endWidget();

?>