<div id="task">
    <?php
            $data['problem'] = $problem;
            $data['answers'] = $answers;
            $data['list']    = $list;
            $data['testProblemId']    = $testProblemId;
            $data['additionalTaskNumber']   = $additionalTaskNumber;
            
            $this->renderPartial('_task', $data, false, true);?>
</div>