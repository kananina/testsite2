<?php

/**
 * This is the model class for table "problems".
 *
 * The followings are the available columns in table 'problems':
 * @property integer $problemId
 * @property integer $themeId
 * @property integer $testProblemId
 * @property string $egePartId
 * @property string $text
 * @property string $problemName
 * @property integer $mistake
 * @property integer $learningId
 * @property integer $testId
 *
 * The followings are the available model relations:
 * @property ProblemAnswers[] $problemAnswers
 * @property Themes $theme
 * @property Tests $test
 * @property Learning $learning
 */
class Problems extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Problems the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'problems';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('themeId, testProblemId, egePartId, text, problemName, mistake, learningId, testId', 'required'),
			array('themeId, testProblemId, mistake, learningId, testId', 'numerical', 'integerOnly'=>true),
			array('egePartId, problemName', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('problemId, themeId, testProblemId, egePartId, text, problemName, mistake, learningId, testId', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'problemAnswers' => array(self::HAS_MANY, 'ProblemAnswers', 'problemId'),
			'theme' => array(self::BELONGS_TO, 'Themes', 'themeId'),
			'test' => array(self::BELONGS_TO, 'Tests', 'testId'),
			'learning' => array(self::BELONGS_TO, 'Learning', 'learningId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'problemId' => 'Problem',
			'themeId' => 'Theme',
			'testProblemId' => 'Test Problem',
			'egePartId' => 'Ege Part',
			'text' => 'Text',
			'problemName' => 'Problem Name',
			'mistake' => 'Mistake',
			'learningId' => 'Learning',
			'testId' => 'Test',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('problemId',$this->problemId);
		$criteria->compare('themeId',$this->themeId);
		$criteria->compare('testProblemId',$this->testProblemId);
		$criteria->compare('egePartId',$this->egePartId,true);
		$criteria->compare('text',$this->text,true);
		$criteria->compare('problemName',$this->problemName,true);
		$criteria->compare('mistake',$this->mistake);
		$criteria->compare('learningId',$this->learningId);
		$criteria->compare('testId',$this->testId);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}