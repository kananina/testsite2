<?php


class ProblemAnswers extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ProblemAnswers the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'problemAnswers';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('problemId, rightAnswer, wrongAnswer1, additionalTask1, wrongAnswer2, additionalTask2, wrongAnswer3, additionalTask3', 'required'),
			array('problemId, additionalTask1, additionalTask2, additionalTask3', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('problemId, rightAnswer, wrongAnswer1, additionalTask1, wrongAnswer2, additionalTask2, wrongAnswer3, additionalTask3', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'problem' => array(self::BELONGS_TO, 'Problems', 'problemId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'problemId' => 'Problem',
			'rightAnswer' => 'Right Answer',
			'wrongAnswer1' => 'wrong Answer1',
			'additionalTask1' => 'Problem Id1',
			'wrongAnswer2' => 'wrong Answer2',
			'additionalTask2' => 'Problem Id2',
			'wrongAnswer3' => 'wrong Answer3',
			'additionalTask3' => 'Problem Id3',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('problemId',$this->problemId);
		$criteria->compare('rightAnswer',$this->rightAnswer,true);
		$criteria->compare('wrongAnswer1',$this->wrongAnswer1,true);
		$criteria->compare('additionalTask1',$this->additionalTask1);
		$criteria->compare('wrongAnswer2',$this->wrongAnswer2,true);
		$criteria->compare('additionalTask2',$this->additionalTask2);
		$criteria->compare('wrongAnswer3',$this->wrongAnswer3,true);
		$criteria->compare('additionalTask3',$this->additionalTask3);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}